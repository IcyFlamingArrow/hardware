# Copyright 2019-2021 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup pn=SPIRV-LLVM-Translator tag=v${PV} ] \
    cmake \
    alternatives

SUMMARY="LLVM/SPIR-V Bi-Directional Translator"
DESCRIPTION="
Library and tool for translation between LLVM IR and SPIR-V.
"

LICENCES="UoI-NCSA"
SLOT="$(ever major)"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/llvm:${SLOT}
"

LLVM_PREFIX="/usr/$(exhost --target)/lib/llvm/${SLOT}"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=TRUE
    -DCMAKE_INSTALL_PREFIX:STRING="${LLVM_PREFIX}"
    -DLLVM_DIR:PATH="${LLVM_PREFIX}"/lib/cmake/llvm
    -DLLVM_BUILD_TOOLS:BOOL=TRUE
)
