# Copyright 2009-2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2010-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2010 Piotr Jaorszyński <p.jaroszynski@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# Note to my future self: Find status information here: https://www.nvidia.com/object/unix.html

require makeself systemd-service udev-rules

export_exlib_phases src_unpack src_prepare src_configure src_install pkg_postinst

SUMMARY="NVIDIA X11 driver and GLX libraries"
HOMEPAGE="https://www.nvidia.com/"

DOWNLOADS="
    listed-only:
        https://us.download.nvidia.com/XFree86/Linux-x86_64/${PV}/NVIDIA-Linux-x86_64-${PV}.run
        https://download.nvidia.com/XFree86/Linux-x86_64/${PV}/NVIDIA-Linux-x86_64-${PV}.run
"

LICENCES="NVIDIA"
SLOT="0"
MYOPTIONS="
    doc
    tools [[ description = [ Install nvidia-settings GUI application ] ]]
    wayland
"

DEPENDENCIES="
    run:
        dev-libs/libglvnd[>=0.1.1] [[ note = [ NVIDIA_Changelog says it needs at least commit b7d7542 ] ]]
        x11-libs/libX11
        x11-libs/libXau
        x11-libs/libXext
        tools? (
            dev-libs/atk
            dev-libs/glib:2
            x11-libs/gtk+:3
            x11-libs/pango
        )
        wayland? ( sys-libs/egl-wayland )
    suggestion:
        x11-libs/libvdpau[>=0.3] [[ note = [ nvidia-drivers installs a VDPAU driver ] ]]
"

NVIDIA_DRIVER_MODULES=(
    "nvidia /kernel/drivers/video"
    "nvidia-drm /kernel/drivers/video"
    "nvidia-modeset /kernel/drivers/video"
    "nvidia-uvm /kernel/drivers/video"
)

nvidia-drivers_generate_dkms_modules() {
    local module n=0
    for module in "${NVIDIA_DRIVER_MODULES[@]}"; do
        local args=(${module})
        echo "BUILT_MODULE_NAME[${n}]=\"${args[0]}\""
        echo "DEST_MODULE_LOCATION[${n}]=\"${args[1]}\""
        let ++n
    done
}

nvidia-drivers_src_unpack() {
    unpack_makeself NVIDIA-Linux-x86_64-${PV}.run
}

nvidia-drivers_src_prepare() {
    local desktop_path=usr/share/applications/nvidia-settings.desktop
    desktop_path=nvidia-settings.desktop

    default

    edo sed \
        -e "s:__UTILS_PATH__:/usr/$(exhost --target)/bin:" \
        -e 's:__PIXMAP_PATH__:/usr/share/pixmaps:' \
        -i "${desktop_path}"
}

nvidia-drivers_src_configure() {
    moduledir=/usr/$(exhost --target)/lib/xorg/modules

    # configure dkms
    local dkms_modules=$(nvidia-drivers_generate_dkms_modules | sed -e 's/$/\\/g;$s/\\$//')

    edo sed \
        -e "s:__VERSION_STRING:${PV}:" \
        -e "s:__JOBS:${EXJOBS}:" \
        -e "s:__EXCLUDE_MODULES::" \
        -e 's/^__DKMS_MODULES$/#&/' \
        -e "/^#__DKMS_MODULES$/a${dkms_modules}" \
        -i kernel/dkms.conf
}

src_install_32bit() {
    # parse the .manifest file to figure out where to install stuff
    while read line ; do
        line=( $line )
        case ${line[2]} in
            UTILITY_LIB|ENCODEAPI_LIB|NVCUVID_LIB)
                if [[ ${line[3]} == "COMPAT32" ]] ; then
                    exeinto /usr/$(exhost --target)/lib
                    doexe ${line[0]}
                fi
                ;;
            UTILITY_LIB_SYMLINK|ENCODEAPI_LIB_SYMLINK|NVCUVID_LIB_SYMLINK|OPENGL_SYMLINK)
                if [[ ${line[3]} == "COMPAT32" ]] ; then
                    dosym ${line[4]} /usr/$(exhost --target)/lib/${line[0]}
                fi
                ;;
            OPENGL_LIB|VDPAU_LIB|CUDA_LIB|OPENCL_LIB|NVIFR_LIB)
                if [[ ${line[3]} == "COMPAT32" ]] ; then
                    exeinto /usr/$(exhost --target)/lib/${line[4]}
                    doexe ${line[0]}
                fi
                ;;
            VDPAU_SYMLINK|CUDA_SYMLINK|OPENCL_LIB_SYMLINK)
                if [[ ${line[3]} == "COMPAT32" ]] ; then
                    dosym ${line[5]} /usr/$(exhost --target)/lib/${line[4]}/${line[0]}
                fi
                ;;
            TLS_LIB)
                if [[ ${line[3]} == "COMPAT32" ]] ; then
                    exeinto /usr/$(exhost --target)/lib/${line[5]}
                    doexe ${line[0]}
                fi
                ;;
        esac
    done < .manifest

    local modules=( egl opengl )
    for module in "${modules[@]}"; do
        edo mv "${IMAGE}"/usr/$(exhost --target)/lib/MODULE\:${module}/* \
            "${IMAGE}"/usr/$(exhost --target)/lib/
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/MODULE\:${module}
    done

}

src_install_64bit() {
    # we do not install nvidia-installer, remove the manpage
    edo sed -e '/nvidia-installer\.1\.gz/d' -i .manifest

    # use only gtk3 for nvidia-settings in recent versions
    edo sed -e '/libnvidia-gtk2.so/d' -i .manifest

    # use the one from sys-libs/egl-wayland
    edo sed -e '/libnvidia-egl-wayland.so/d' -i .manifest

    # remove disabled stuff
    if ! option tools ; then
        edo sed \
            -e '/libnvidia-gtk3.so/d' \
            -e '/nvidia-settings/d' \
            -i .manifest
    fi

    if ! option wayland ; then
        edo sed \
            -e '/10_nvidia_wayland.json/d' \
            -i .manifest
    fi

    # parse the .manifest file to figure out where to install stuff
    while read line ; do
        line=( $line )
        case ${line[2]} in
            UTILITY_BINARY)
                dobin ${line[0]}
                ;;
            MANPAGE)
                edo gunzip ${line[0]}
                doman ${line[0]%.gz}
                ;;
            XMODULE_SHARED_LIB|GLX_MODULE_SHARED_LIB)
                exeinto ${moduledir}/${line[3]}
                doexe ${line[0]}
                ;;
            XMODULE_SYMLINK|GLX_MODULE_SYMLINK)
                dosym ${line[4]} ${moduledir}/${line[3]}${line[0]}
                ;;
            XLIB_SHARED_LIB|UTILITY_LIB|ENCODEAPI_LIB|NVCUVID_LIB)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    dolib.so ${line[0]}
                fi
                ;;
            XLIB_SYMLINK|UTILITY_LIB_SYMLINK|ENCODEAPI_LIB_SYMLINK|NVCUVID_LIB_SYMLINK|OPENGL_SYMLINK)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    dosym ${line[4]} /usr/$(exhost --target)/lib/${line[0]}
                fi
                ;;
            OPENGL_LIB|VDPAU_LIB|CUDA_LIB|OPENCL_LIB|NVIFR_LIB)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    exeinto /usr/$(exhost --target)/lib/${line[4]}
                    doexe ${line[0]}
                fi
                ;;
            VDPAU_SYMLINK|CUDA_SYMLINK|OPENCL_LIB_SYMLINK)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    dosym ${line[5]} /usr/$(exhost --target)/lib/${line[4]}/${line[0]}
                fi
                ;;
            TLS_LIB)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    exeinto /usr/$(exhost --target)/lib/${line[5]}
                    doexe ${line[0]}
                fi
                ;;
            WINE_LIB)
                if [[ ${line[3]} == "NATIVE" ]] ; then
                    exeinto /usr/$(exhost --target)/lib/nvidia/wine
                    doexe ${line[0]}
                fi
                ;;
            DOT_DESKTOP)
                if option tools ; then
                    insinto /usr/share/applications
                    doins ${line[0]}
                fi
                ;;
            APPLICATION_PROFILE)
                if option tools ; then
                    insinto /usr/share/nvidia
                    doins ${line[0]}
                fi
                ;;
            XORG_OUTPUTCLASS_CONFIG)
                insinto /usr/share/X11/xorg.conf.d
                doins ${line[0]}
                ;;
            CUDA_ICD)
                insinto /etc/OpenCL/vendors
                doins ${line[0]}
                ;;
            VULKAN_ICD_JSON)
                insinto /usr/share/vulkan/${line[3]}
                doins ${line[0]}
                ;;
            GLVND_EGL_ICD_JSON)
                insinto /usr/share/glvnd/egl_vendor.d
                doins ${line[0]}
                ;;
            EGL_EXTERNAL_PLATFORM_JSON)
                insinto /usr/share/egl/egl_external_platform.d
                doins ${line[0]}
                ;;
            FIRMWARE)
                insinto /usr/$(exhost --target)/lib/firmware
                doins ${line[0]}
                ;;
            SYSTEMD_SLEEP_SCRIPT)
                exeinto /usr/$(exhost --target)/lib/systemd/system-sleep
                doexe ${line[0]}
                ;;
            SYSTEMD_UNIT)
                insinto ${SYSTEMDSYSTEMUNITDIR}
                doins ${line[0]}
                ;;
        esac
    done < .manifest

    local modules=( egl opengl ngx raytracing optix )
    for module in "${modules[@]}"; do
        edo mv "${IMAGE}"/usr/$(exhost --target)/lib/MODULE\:${module}/* \
            "${IMAGE}"/usr/$(exhost --target)/lib/
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/MODULE\:${module}
    done

    # GBM loader symlink as mesa gbm-backends-path option defaults to $libdir/gbm
    insinto /usr/$(exhost --target)/lib/gbm
    dosym ../libnvidia-allocator.so.1 /usr/$(exhost --target)/lib/gbm/nvidia-drm_gbm.so

    # nvidia-settings icon
    if option tools ; then
        insinto /usr/share/pixmaps
        doins nvidia-settings.png
    fi

    # kernel module source
    local modules_sources=/usr/src/${PNV}
    insinto ${modules_sources}
    doins -r kernel/*

    local arch=x86_64

    edo mv "${IMAGE}"${modules_sources}/nvidia/nv-kernel{,-${arch}}.o_binary
    edo sed \
        -e "s/nv-kernel\\.o/nv-kernel-\$(ARCH).o/" \
        -i "${IMAGE}"${modules_sources}/nvidia/nvidia.Kbuild

    edo mv "${IMAGE}"${modules_sources}/nvidia-modeset/nv-modeset-kernel{,-${arch}}.o_binary
    edo sed \
        -e "s/nv-modeset-kernel\\.o/nv-modeset-kernel-\$(ARCH).o/" \
        -i "${IMAGE}"${modules_sources}/nvidia-modeset/nvidia-modeset.Kbuild

    hereenvd 40nvidia <<EOF
LDPATH=/usr/@TARGET@/lib/vdpau
EOF

    if option tools ; then
        insinto /etc/X11/xinit/xinitrc.d
        hereins 95-nvidia-settings <<EOF
#!/bin/sh

/usr/$(exhost --target)/bin/nvidia-settings --load-config-only
EOF
        edo chmod ugo+x "${IMAGE}"/etc/X11/xinit/xinitrc.d/95-nvidia-settings
    fi

    # module loading configuration
    insinto /etc/modprobe.d
    doins "${FILES}"/nvidia.conf

    # create devices nodes (e.g. /dev/nvidia-uvm{,-tools})
    install_udev_files

    # machine-readable list of supported GPUs and features
    dodoc supported-gpus/supported-gpus.json
}

nvidia-drivers_src_install() {
    # docs
    dodoc pkg-history.txt NVIDIA_Changelog README.txt
    if option doc; then
        docinto html
        dodoc -r html/*
    fi

    local arch=$(exhost --target)

    case "${arch}" in
        i*86-*)
            src_install_32bit
            ;;
        *)
            src_install_64bit
            ;;
    esac
}

nvidia-drivers_pkg_postinst() {
    # Ensure that libglvnd is selected after the alternative got removed from nvidia-drivers
    # added 2019-01-27/version 415.23, remove after a transitionary period
    nonfatal edo eclectic opengl update --ignore nvidia-drivers

    elog "The kernel modules source is installed into /usr/src/${PNV}/"
    elog "You will have to compile it by hand. Make sure the 'nvidia' kernel module is loaded."
    elog "Make sure you use the bfd linker to link the module, using gold"
    elog "seems to result in a broken module, so don't forget to run"
    elog "'eclectic ld set bfd' as root."
}

